
#############################################################################
# マクロ定義
#############################################################################

# 共通マクロ
include Makefile.inc
sinclude Makefile.user.inc


# サブディレクトリ 
SUBDIRS:=doga sound

# エンコード関連
AVS2WAV:=$(ENC_UTIL_DIR)/avs2wav.exe
NEROAACENC:=$(ENC_UTIL_DIR)/neroAacEnc_SSE.exe
X264:=$(ENC_UTIL_DIR)/x264_x64.exe
MUXER:=$(ENC_UTIL_DIR)/muxer.exe
REMUXER:=$(ENC_UTIL_DIR)/remuxer.exe

AOPTION=-cbr 256000 -lc
AOPTION_ECO=-cbr 64000 -lc
VOPTION=--opencl --bitrate 1500 --vbv-maxrate 3500 --vbv-bufsize 3400 --profile high --preset slower --partitions all --me "umh" --subme 10 --merange 24 --ref 6 --bframes 5 --b-pyramid normal --no-dct-decimate --no-fast-pskip --qcomp 0.70 --qpstep 20 
VOPTION_ECO=--opencl --bitrate 230 --profile main --preset slower --8x8dct --partitions "p8x8,b8x8,i8x8,i4x4" --me "umh" --subme 10 --merange 24 --ref 6 --bframes 5 --b-pyramid normal --no-dct-decimate --no-fast-pskip --qcomp 0.70 --qpstep 20 
VOPT_FAST=--analyse none --subme 1 --me dia
VOPT_SLOW=
FPS=24

IN_AVS=main.avs
VER_AVS=version.avs
OUT_MP4=_release/kosys.mp4
TMPA1=_release/kosys.tmp.m4a
TMPV1=_release/kosys.tmp.264
TMPV2=_release/kosys.tmp.mp4
OUT_MP4_PRE=_release/kosys_pre.mp4
TMPV1_PRE=_release/kosys_pre.tmp.264
TMPV2_PRE=_release/kosys_pre.tmp.mp4
OUT_MP4_ECO=_release/kosys_eco.mp4
TMPA1_ECO=_release/kosys_eco.tmp.m4a
TMPV1_ECO=_release/kosys_eco.tmp.264
TMPV2_ECO=_release/kosys_eco.tmp.mp4

#############################################################################
# Makeルール
#############################################################################

.PHONY: all $(SUBDIRS)
all: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@


.PHONY: release
release: $(OUT_MP4)
$(OUT_MP4): $(TMPA1) $(TMPV1) $(TMPV2)
	$(REMUXER) -i $(TMPV2) -i $(TMPA1) -o $(OUT_MP4)

$(TMPV2): $(TMPV1)
	$(MUXER) -i $(TMPV1)?fps=$(FPS) -o $(TMPV2)


$(TMPV1): $(wildcard *.avs) $(wildcard doga/_output/*.avi) version
	$(X264) $(VOPTION) --pass 1 $(VOPT_FAST) -o $(TMPV1) $(IN_AVS)
	$(X264) $(VOPTION) --pass 2 $(VOPT_SLOW) -o $(TMPV1) $(IN_AVS)
	
	
$(TMPA1): $(wildcard *.avs) $(wildcard doga/_output/*.avi) $(wildcard sound/*.wav)  $(wildcard sound/*.mp3)
	$(AVS2WAV) $(IN_AVS)  - |$(NEROAACENC) $(AOPTION) -if - -of $(TMPA1)
	

.PHONY: preview
preview: $(OUT_MP4_PRE)
$(OUT_MP4_PRE): $(TMPA1) $(TMPV1_PRE) $(TMPV2_PRE)
	$(REMUXER) -i $(TMPV2_PRE) -i $(TMPA1) -o $(OUT_MP4_PRE)

$(TMPV2_PRE): $(TMPV1_PRE)
	$(MUXER) -i $(TMPV1_PRE)?fps=$(FPS) -o $(TMPV2_PRE)

$(TMPV1_PRE): $(TMPA1) $(wildcard *.avs) $(wildcard doga/_output/*.avi) version
	$(X264) $(VOPTION) $(VOPT_FAST) -o $(TMPV1_PRE) $(IN_AVS)
	

eco: $(OUT_MP4_ECO)
$(OUT_MP4_ECO): $(OUT_MP4) $(TMPA1_ECO)
	$(X264) $(VOPTION_ECO) --pass 1 $(VOPT_FAST) --video-filter resize:352,200,1:1:lanczos -o $(TMPV1_ECO) $(OUT_MP4)
	$(X264) $(VOPTION_ECO) --pass 2 $(VOPT_SLOW) --video-filter resize:352,200,1:1:lanczos -o $(TMPV1_ECO) $(OUT_MP4)
	$(MUXER) -i $(TMPV1_ECO)?fps=$(FPS) -o $(TMPV2_ECO)
	$(REMUXER) -i $(TMPV2_ECO) -i $(TMPA1_ECO) -o $(OUT_MP4_ECO)
	
$(TMPA1_ECO): $(OUT_MP4)  $(wildcard *.avs) $(wildcard doga/_output/*.avi) $(wildcard doga/_output/*.wav)  $(wildcard doga/_output/*.mp3)
	$(AVS2WAV) $(IN_AVS)  - |$(NEROAACENC) $(AOPTION_ECO) -if - -of $(TMPA1_ECO)

.PHONY: version
version: 
# gitのリビジョン番号を取得する
	echo 'global VERSION="$(shell git describe --abbrev=7 --dirty --always --tags)"' >  $(VER_AVS).tmp
# リビジョン番号が変化していたときのみ上書きする
	test -e $(VER_AVS) || cp -f $(VER_AVS).tmp $(VER_AVS)
	test "$$(md5sum $(VER_AVS).tmp | awk '{ print $$1 }')" != "$$(md5sum $(VER_AVS) | awk '{ print $$1 }')" && cp -f $(VER_AVS).tmp $(VER_AVS) || true
	rm -f $(VER_AVS).tmp

