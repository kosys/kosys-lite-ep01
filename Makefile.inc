# 実行ファイル
AERENDER:=/c/Program\ Files/Adobe/Adobe\ After\ Effects\ CC\ 2015/Support\ Files/aerender.exe
RENDER_OPTS:= -sound ON -OMtemplate LagarithRGBA -mp

# リビジョン番号取得
GIT_VER = $(shell git log -1 --format="%h")

# ディレクトリ取得
CURRENT_DIR=$(abspath .)
PARENT_DIR=$(abspath ..)
BASE_DIR:=$(abspath $(dir $(lastword $(MAKEFILE_LIST))))

# コンポ設定
SCENE_NUM=$(shell basename $(PARENT_DIR))
CUT_NUM=$(shell basename $(CURRENT_DIR))
PROJ_FILE=$(CURRENT_DIR)/$(SCENE_NUM)_$(CUT_NUM).aep
COMP_NAME=_output
OUT_FILE=$(BASE_DIR)/doga/_output/$(SCENE_NUM)_$(CUT_NUM).avi

#共通処理
define STANDERD_RENDER
     $(AERENDER) -project "$(PROJ_FILE)" -comp "$(COMP_NAME)" -output "$(OUT_FILE)" $(RENDER_OPTS)
endef

